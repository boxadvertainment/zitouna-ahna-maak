<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Oups! erreur</title>

        <link rel="stylesheet" href="{{ asset('css/vendor-admin.css') }}">
        <link rel="stylesheet" href="{{ asset('css/admin.css') }}">

        @stack('stylesheets')

    </head>

    <body class="nav-md" style="font-family: sans-serif">
        <div class="container body">
            <div class="main_container">

                @yield('main_container')

            </div>
        </div>

        <script src="{{ asset("js/vendor-admin.js") }}"></script>
        <script src="{{ asset("js/admin.js") }}"></script>

        @stack('scripts')

    </body>
</html>
