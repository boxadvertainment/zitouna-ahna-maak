@extends('layout')

@push('stylesheets')

@section('class', 'how-to')

@section('main_container')

<section class="section">
    <div class="container" dir="ltr">
      <div class="row justify-content-center p-md-5">

        <div class="how-to-content p-md-5">
            <h1 class="mb-5"> :باش تشارك معانا</h1>

            <div class="line line1">  .عمّر المعطيات الخاصة بيك <img src="images/list-1.png" alt="" width="30px"></div>
            <div class="line line2"> رتّب الفروع من الأقرب ليك إلى الأبعد  <img src="images/list-2.png" alt="" width="30px"> </div>
            <div class="line line3"> سجّل مشاركتك باش تدخل في القرعة <img src="images/list-3.png" alt="" width="30px"></div>


        </div>
      </div>
    </div>
</section>




@endsection
