@extends('admin.layouts.blank')

@section('main_container')

    <div class="right_col" role="main">
        <div class="">
            <div class="row top_tiles">
                <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12 col-md-offset-2">
                    <div class="tile-stats bg-green">
                        <div class="icon"style="color:#0f6655"><i class="fa fa-user"></i></div>
                        <div class="count">{{ $nbParticipant }}</div>
                        <h3 style="color:#0f6655">Participants</h3>
                        <p>Total numbers of participants.</p>
                    </div>
                </div>
                <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="tile-stats bg-green">
                        <div class="icon"style="color:#0f6655"><i class="fa fa-gamepad"></i></div>
                        <div class="count">{{ $nbGames }}</div>
                        <h3 style="color:#0f6655">Game</h3>
                        <p>Total games played</p>
                    </div>
                </div>

            </div>



            <div class="clearfix"></div>


            @if($nbParticipant!=0)
                    <div class="row">

                        @foreach($games as $key => $item)


                        <div class="col-sm-4 col-md-3">
                            <div class="thumbnail" style="height: 140px;">

                                <div style="padding:20px;">
                                    <div class="row post-detail">
                                        <h4>
                                            <a href="#">{{$key}} - {{ $item->name }}</a> <br>
                                            <small>{{ $item->tel }} —
                                            {{ $item->city }} </small></h4>

                                            <p>Réponse :  {!!  ($item->won) ? '<span class="label label-success"> Correcte' : ' <span class="label label-danger">Fausse'  !!} </span></p>
                                    </div>

                                </div>
                            </div>
                        </div>
                        @endforeach
                </div>
            @else
                <h3 class="text-center">Aucune post!!!</h3>
            @endif


        </div>
    </div>

@endsection

@push('scripts')
<script>


</script>

@endpush
