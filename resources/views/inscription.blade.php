@extends('layout')

@push('stylesheets')

    @section('class', 'inscription')

@section('main_container')

    <section class="section form">
        <div class="form-wrapper">
            <form id="inscription" action="{{ url('saveForm') }}" method="post" data-toggle="validator" lang="fr"
                  class="">
                {{ csrf_field() }}


                <div class="row">
                    <div class="col-12">
                        <h1 class="mb-5">نتعرفو عليك </h1>
                    </div>
                    <div class="col-12 mt-5">
                        <div class="form-group">
                            <label for="inputName">الاسم واللقب</label>
                            <input id="inputName" type="text" class="form-control" name="name"
                                   placeholder="الاسم واللقب" required title="Ce champ est obligatoire">
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <label for="inputTel">رقم الهاتف</label>
                            <input id="inputTel" type="text" class="form-control" name="tel" placeholder="رقم الهاتف"
                                   required title="Num. tél. incorrect" maxlength="8" pattern="^[0-9]{8}$"
                                   onkeypress="return (event.charCode > 47 && event.charCode < 58)">
                        </div>

                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <label for="inputCity">مدينة السكن</label>
                            <select name="city" id="inputCity" class="form-control" required>
                                <option value="-1" disabled="" selected>المدينة</option>
                                <option value="Ariana">Ariana</option>
                                <option value="Béja">Béja</option>
                                <option value="Ben Arous">Ben Arous</option>
                                <option value="Bizerte">Bizerte</option>
                                <option value="Gabès">Gabès</option>
                                <option value="Gafsa">Gafsa</option>
                                <option value="Jendouba">Jendouba</option>
                                <option value="Kairouan">Kairouan</option>
                                <option value="Kasserine">Kasserine</option>
                                <option value="Kébili">Kébili</option>
                                <option value="La Manouba">La Manouba</option>
                                <option value="Le Kef">Le Kef</option>
                                <option value="Mahdia">Mahdia</option>
                                <option value="Médenine">Médenine</option>
                                <option value="Monastir">Monastir</option>
                                <option value="Nabeul">Nabeul</option>
                                <option value="Sfax">Sfax</option>
                                <option value="Sidi Bouzid">Sidi Bouzid</option>
                                <option value="Siliana">Siliana</option>
                                <option value="Sousse">Sousse</option>
                                <option value="Tataouine">Tataouine</option>
                                <option value="Tozeur">Tozeur</option>
                                <option value="Tunis">Tunis</option>
                                <option value="Zaghouan">Zaghouan</option>
                            </select>
                        </div>

                    </div>

                    <div class="col-12 mt-5">
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" name="submit" value="Valider">
                        </div>

                    </div>

                </div>
            </form>
        </div>

        <div class="gameCanvas text-center hide" dir="ltr">
            <form id="gameForm" action="{{ url('saveGame') }}" method="post" lang="fr"
                  class="">
                {{ csrf_field() }}
                <input type="hidden" name="game" id="inpuGame">
                <input type="hidden" name="tel" id="inpuPhone">
                <input type="hidden" name="current" id="inpuCurrent">
            <h1>اناهو أقرب فرع ليك <br> <small>
                    رتبهم من 1 إلى 3
                </small></h1>

            <div class="gameArea">

                <div class="bankItem bankItem-1" data-order="">
                    <img src="{{ asset("images/bank-icn.png") }}" alt="">
                    <div class="bank__name">NABEUL PL. MARTYRS'</div>
                </div>
                <div class="bankItem bankItem-2" data-order="">
                    <img src="{{ asset("images/bank-icn.png") }}" alt="">
                    <div class="bank__name">marsa</div>
                </div>
                <div class="bankItem bankItem-3" data-order="">
                    <img src="{{ asset("images/bank-icn.png") }}" alt="">
                    <div class="bank__name">marsa</div>
                </div>

                <div class="target-wrapper d-flex justify-content-center">
                    <div class="target target-1" id="t1" data-index="3"></div>
                    <div class="target target-2" id="t2" data-index="2"></div>
                    <div class="target target-3" id="t3" data-index="1"></div>
                </div>

                <div class="w-100 mt-5">
                    <button class="mt-5 btn btn-primary disabled" disabled="" id="checkResut">Valider</button>
                </div>


            </div>

            <div class="bankList"></div>
            </form>
        </div>

        <div class="hide msg-wrapper text-center" dir="rtl">
            <p>الإجابة صحيحة ! نشكروك على المشاركة
                <br>
                ونعلموك ألي تنجم تعاود تلعب غدوة بش تقوي حظوظك في الربح !
                <br>

                تبعنا على <a href="https://www.facebook.com/BanqueZitouna" target="_blank" class="latin-font">Page
                    Facebook</a>  متاع مصرف الزيتونة  بش تعرف إذا كنت
                <br>
                من الرابحين </p>
        </div>
        <div class="hide msg-wrapper-error text-center" dir="rtl">
            <p> الإجابة خاطئة ! <br> نشكروك على المشاركة <br> ونعلموك ألي تنجم تعاود تلعب غدوة </p>
        </div>

    </section>




@endsection
