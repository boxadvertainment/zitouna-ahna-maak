<!doctype html>
<!--[if lt IE 9]>      <html class="no-js lt-ie9" lang="fr" dir="rtl"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr" dir="rtl"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title'){{ Config::get('app.name') }}</title>

    @include('partials.socialMetaTags', [
        'title' => '',
        'description' => ""
    ])

    <!-- Metta CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Use RealFaviconGenerator.net to generate favicons  -->
    <link rel="shortcut icon" href="/favicon.ico" />
    {{-- <link rel="shortcut icon" type="image/png" href="/favicon.png"> --}}


    <link rel="stylesheet" href="{{ asset('css/main.css') }}">

    <script src="https://api.tiles.mapbox.com/mapbox-gl-js/v1.6.0/mapbox-gl.js"></script>
    <link href="https://api.tiles.mapbox.com/mapbox-gl-js/v1.6.0/mapbox-gl.css" rel="stylesheet" />
    <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-language/v0.10.0/mapbox-gl-language.js'></script>

    @stack('stylesheets')

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        BASE_URL    = '{{ url('/') }}';
        CURRENT_URL = '{{ request()->url() }}';
        FB_APP_ID   = '{{ env('FACEBOOK_APP_ID') }}';
        APP_ENV     = '{{ app()->environment() }}';
    </script>
    <script src="{{ asset("js/modernizr.js") }}"></script>
</head>
<body class="@yield('class')">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-15479006-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-15479006-1');
</script>

    <!--[if lt IE 9]>
    <div class="alert alert-dismissible outdated-browser show" role="alert">
        <h6>Votre navigateur est obsolète !</h6>
        <p>Pour afficher correctement ce site et bénéficier d'une expérience optimale, nous vous recommandons de mettre à jour votre navigateur.
            <a href="http://outdatedbrowser.com/fr" class="update-btn" target="_blank">Mettre à jour maintenant </a>
        </p>
        <a href="#" class="close-btn" title="Fermer" data-dismiss="alert" aria-label="Fermer">&times;</a>
    </div>
    <![endif]-->

    <!--  Loader -->
    <div class="loader">
        <div class="spinner">
            <small>   Veuillez Patienter </small>
        </div>
    </div>

    <header class="header clearfix">
        <div class="container">
            <div class="logo pull-right">
                <a href="/" title="Accueil"><img src="{{ asset("images/logo-zt.png") }}" id="mainLogo" alt=""></a>
            </div>

            <div class="tag-line">
                <img src="{{ asset("images/tagline.png") }}" alt="">
            </div>

        </div>
    </header>
    <div class="container-fluid">
        <div class="row align-content-stretch app-wrapper">
        <!-- App Wrapper -->
            <div class="col-12 col-lg-3 p-0 map-container" dir="ltr" >
                <div id="map" style="width: 100%;height: 100%;min-height: 150px;" dir="ltr"></div>
            </div>

            <div class="col-12 col-lg page-container">
                @yield('main_container')
                <footer class="align-self-end" dir="ltr">
                    <div class="container latin-font">
                        <div class="row">

                            <div class="col-12 links-menu text-left">
{{--                                <a href="/comment-participer">Comment participer ?</a>    |    <a href="/reglement.pdf">Règlement du jeu</a>    |    <a href="/gain">Gain</a>    <span class="justify-content-center d-none d-md-inline">| </span>  --}}
                                <span class="d-none d-md-inline-block copyright">© 2019 Banque Zitouna </span>
                            </div>

                        </div>
                    </div>
                </footer>
            </div>
        </div>

    <!-- /#wrapper -->
    </div>




    <div class="modal fade" dir="ltr" id="mapModal" tabindex="-1" role="dialog" aria-labelledby="mapModalTitle" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content bg-primary">
                <div class="modal-header">
                    <h5 class="modal-title" id="mapModalTitle">Votre position</h5>
                </div>
                <div class="modal-body p-0">
                    <div id="mapPosition" style="width: 100%; height: 400px"></div>
                </div>
                <div class="modal-footer text-center justify-content-center">
                    <a href="#" id="confirmPosition" class="btn btn-secondary text-primary">Je confirme ma position</a>
                </div>
            </div>
        </div>
    </div>


    <!-- Scripts -->
    <script src="{{ asset("js/vendor.js") }}"></script>
    <script src="{{ asset("js/plugins-front.js") }}"></script>
    <script src="{{ asset("js/plugins.js") }}"></script>
    <script src="{{ asset('js/TweenMax.min.js') }}"></script>
    <script src="{{ asset('js/jigLite.min.js') }}"></script>
    <script src="{{ asset('js/Draggable.min.js') }}"></script>
    <script src="{{ asset("js/main.js") }}"></script>

    @stack('scripts')

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    {{--<script>--}}
        {{--(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){--}}
                    {{--(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),--}}
                {{--m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)--}}
        {{--})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');--}}

        {{--ga('create', '{{  env('GOOGLE_ANALYTICS') }}', 'auto');--}}
        {{--ga('send', 'pageview');--}}
    {{--</script>--}}

</body>
</html>
