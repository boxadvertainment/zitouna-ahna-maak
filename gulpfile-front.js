var elixir = require('laravel-elixir');
require('./elixir-extensions');

elixir(function(mix) {

    mix
        .sass('main.scss')
        .babel([
            'main.js'
        ], 'public/js/main.js')

        /*******************     scripts front End start here          ******************/
        .scripts([
            // bower:js
            'bower_components/jquery/dist/jquery.js',
            'bower_components/es6-promise/es6-promise.js',
            'bower_components/sweetalert2/dist/sweetalert2.js',
            // endbower
        ], 'public/js/plugins-front.js', 'bower_components')
        /*******************     styles front End start here          ******************/
        .styles([
            // bower:css
            'bower_components/animate.css/animate.css',
            'bower_components/sweetalert2/dist/sweetalert2.css',
            // endbower
            'bower_components/gentelella/vendors/font-awesome/css/font-awesome.min.css',
        ], 'public/css/plugins-front.css', 'bower_components')


});
