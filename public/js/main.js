'use strict';

var agencesLocation = [{ 'name': 'AGENCE CENTRALE', 'latitude': '36.841318', 'longitude': '10.29575299999999' }, { 'name': 'Souk Lahad', 'latitude': '33.775243', 'longitude': '8.856323' }, { 'name': 'Regueb', 'latitude': '34.856935', 'longitude': '9.785065' }, { 'name': 'ALI DARGOUTH', 'latitude': '36.795802', 'longitude': '10.182871' }, { 'name': 'ENNACER', 'latitude': '36.861734', 'longitude': '10.16463299999998' }, { 'name': 'LA MARSA', 'latitude': '36.879627', 'longitude': '10.327622' }, { 'name': 'SOUSSE SENGHOR', 'latitude': '35.839505', 'longitude': '10.622395' }, { 'name': 'SOUSSE 14 JANVIER', 'latitude': '35.80928078396995', 'longitude': '10.625424527343739' }, { 'name': 'SFAX BOULILA', 'latitude': '34.74268189454456', 'longitude': '10.7539935342636' }, { 'name': 'SFAX ARIANA', 'latitude': '34.754624', 'longitude': '10.762374000000023' }, { 'name': 'LE KRAM', 'latitude': '36.835108', 'longitude': '10.316763' }, { 'name': 'BAB BNET', 'latitude': '36.803226', 'longitude': '10.165655' }, { 'name': 'MAHDIA', 'latitude': '35.504821', 'longitude': '11.063537999999994' }, { 'name': 'MEGRINE', 'latitude': '36.768145', 'longitude': '10.23965499999997' }, { 'name': 'SIDI BOUSAID', 'latitude': '36.869969', 'longitude': '10.344519' }, { 'name': 'AOUINA', 'latitude': '36.854808', 'longitude': '10.257438' }, { 'name': 'MOHAMED V', 'latitude': '36.820479', 'longitude': '10.181235' }, { 'name': 'MONASTIR', 'latitude': '35.773729', 'longitude': '10.831787' }, { 'name': 'MOUROUJ', 'latitude': '36.721107', 'longitude': '10.215546' }, { 'name': 'MSAKEN', 'latitude': '35.744492', 'longitude': '10.586423' }, { 'name': 'EZZOUHOUR', 'latitude': '36.794344', 'longitude': '10.132648' }, { 'name': 'ETTADHAMEN', 'latitude': '36.839882', 'longitude': '10.116254' }, { 'name': 'SFAX CHIHIA', 'latitude': '34.79548', 'longitude': '10.743201999999997' }, { 'name': 'SFAX GREMDA', 'latitude': '34.760195', 'longitude': '10.741651999999931' }, { 'name': 'MENZAH 8', 'latitude': '36.85701', 'longitude': '10.181782999999996' }, { 'name': 'SIJOUMI', 'latitude': '36.768299', 'longitude': '10.113784' }, { 'name': 'CHARGUIA', 'latitude': '36.853550', 'longitude': '10.204452' }, { 'name': 'WARDIA', 'latitude': '36.775166', 'longitude': '10.180733' }, { 'name': 'NABEUL PL. MARTYRS', 'latitude': '36.456575', 'longitude': '10.737911' }, { 'name': 'HAMMAMET', 'latitude': '36.405404', 'longitude': '10.612724999999955' }, { 'name': 'NABEUL NEAPOLIS', 'latitude': '36.451005', 'longitude': '10.726775999999973' }, { 'name': 'EZZAHRA', 'latitude': '36.739023', 'longitude': '10.315720' }, { 'name': 'BARDO', 'latitude': '36.811698', 'longitude': '10.143052000000012' }, { 'name': 'HRAIRIA', 'latitude': '36.789697', 'longitude': '10.115297000000055' }, { 'name': 'DJERBA HOUMET SOUK', 'latitude': '33.875281', 'longitude': '10.857039999999984' }, { 'name': 'BIZERTE', 'latitude': '37.269242', 'longitude': '9.87253499999997' }, { 'name': 'GABES', 'latitude': '33.885742', 'longitude': '10.100015999999982' }, { 'name': 'AV DE LA LIBERTE', 'latitude': '36.807950', 'longitude': '10.179957' }, { 'name': 'TOZEUR', 'latitude': '33.921304', 'longitude': '8.128463' }, { 'name': 'KEBILI', 'latitude': '33.705769', 'longitude': '8.973118' }, { 'name': 'ZARZIS', 'latitude': '33.503190', 'longitude': '11.110800' }, { 'name': 'TATAOUINE', 'latitude': '32.930750', 'longitude': '10.450074' }, { 'name': 'ZAGHOUAN', 'latitude': '36.399668', 'longitude': '10.145052' }, { 'name': 'LE KEF', 'latitude': '36.171014', 'longitude': '8.705473' }, { 'name': 'HAMMAM LIF', 'latitude': '36.73107093319898', 'longitude': '10.328740023788441' }, { 'name': 'SFAX BEB BHAR', 'latitude': '34.73035166388643', 'longitude': '10.760945820030202' }, { 'name': 'SFAX SAKEIT EDDAYER', 'latitude': '34.76808597657034', 'longitude': '10.774018906768788' }, { 'name': 'BEN AROUS', 'latitude': '36.756617', 'longitude': '10.219917' }, { 'name': 'LAC 2', 'latitude': '36.8489879618931', 'longitude': '10.277037762817372' }, { 'name': 'ARIANA', 'latitude': '36.853279', 'longitude': '10.185280' }, { 'name': 'SFAX PIC VILLE', 'latitude': '34.736517009200014', 'longitude': '10.754814290222157' }, { 'name': 'SILIANA', 'latitude': '36.08738973032177', 'longitude': '9.369909309562672' }, { 'name': 'KƒLIBIA', 'latitude': '36.84646380747434', 'longitude': '11.089682721313466' }, { 'name': 'KASSERINE', 'latitude': '35.170199', 'longitude': '8.839272' }, { 'name': 'SOUSSE BEB BHAR', 'latitude': '35.83112102278709', 'longitude': '10.640198134597767' }, { 'name': 'BEB JAZIRA', 'latitude': '36.79836017639932', 'longitude': '10.175516151603688' }, { 'name': 'ALAIN SAVARY', 'latitude': '36.82527136152579', 'longitude': '10.182334326919545' }, { 'name': 'CENTRE URBAIN NORD', 'latitude': '36.847012', 'longitude': '10.195665' }, { 'name': 'KAIROUAN ', 'latitude': '35.670920', 'longitude': '10.101432' }, { 'name': 'GAFSA', 'latitude': '34.416829', 'longitude': '8.790550' }, { 'name': 'SIDI BOUZID', 'latitude': '35.0383094956317', 'longitude': '9.48808207434081' }, { 'name': 'SOUSSE SAHLOUL', 'latitude': '35.841323', 'longitude': '10.597991' }, { 'name': 'MENZEL TEMIME', 'latitude': '36.77652278502928', 'longitude': '10.988874577697743' }, { 'name': 'JENDOUBA', 'latitude': '36.500910', 'longitude': '8.779541' }, { 'name': 'KSAR HELLAL', 'latitude': '35.647625', 'longitude': '10.885167' }, { 'name': 'GHOMRASSEN', 'latitude': '33.059194', 'longitude': '10.337089' }, { 'name': 'MSAKEN VILLE', 'latitude': '35.729533', 'longitude': '10.579847' }, { 'name': 'BEN GUERDEN', 'latitude': '33.138248', 'longitude': '11.219863000000032' }, { 'name': 'ZI KHEIREDDINE', 'latitude': '36.828938', 'longitude': '10.306532' }, { 'name': 'MONASTIR TAIEB MHIRI', 'latitude': '35.766825', 'longitude': '10.822935' }, { 'name': 'LAC 1', 'latitude': '36.831459', 'longitude': '10.230578' }, { 'name': 'MEDNINE ROUTE TATAOUINE', 'latitude': '33.337996', 'longitude': '10.486327' }, { 'name': 'BORJ CEDRIA', 'latitude': '36.704717', 'longitude': '10.407293' }, { 'name': 'MENZAH I', 'latitude': '36.832457', 'longitude': '10.179787' }, { 'name': 'KSOUR ESSEF', 'latitude': '35.416551', 'longitude': '10.998385' }, { 'name': 'GABéS MENZEL', 'latitude': '33.880497', 'longitude': '10.090669' }, { 'name': 'MANAR II', 'latitude': '36.842675', 'longitude': '10.159326' }, { 'name': 'SFAX SOUKRA', 'latitude': '34.722463', 'longitude': '10.717345' }, { 'name': 'ENFIDHA', 'latitude': '36.133682', 'longitude': '10.376365' }, { 'name': 'MUTUELLE VILLE', 'latitude': '36.835232', 'longitude': '10.166776' }, { 'name': 'SOUKRA', 'latitude': '36.863760', 'longitude': '10.214871' }, { 'name': 'DOUZ', 'latitude': '33.454488', 'longitude': '9.022711' }, { 'name': 'MENZEL BOURGUIBA', 'latitude': '37.154562', 'longitude': '9.794780' }, { 'name': 'SFAX MENZEL CHAKER', 'latitude': '34.752724', 'longitude': '10.713851' }, { 'name': 'SOLIMAN ', 'latitude': '36.694067', 'longitude': '10.494642' }, { 'name': 'BOUMHEL', 'latitude': '36.728418', 'longitude': '10.306260' }, { 'name': 'RAS JEBEL', 'latitude': '37.213763', 'longitude': '10.124819' }, { 'name': 'SFAX ROUTE LAFRANE', 'latitude': '34.758936', 'longitude': '10.733171' }, { 'name': 'SFAX SAKIET EZZIT', 'latitude': '34.805444', 'longitude': '10.761374' }, { 'name': 'MEDNINE', 'latitude': '33.348043', 'longitude': '10.487594' }, { 'name': 'MOKNINE', 'latitude': '35.628945', 'longitude': '10.899732' }, { 'name': 'ELJEM', 'latitude': '35.292699', 'longitude': '10.708686' }, { 'name': 'Boussalem', 'latitude': '36.609501', 'longitude': '8.972647' }, { 'name': 'Hammam Sousse', 'latitude': '35.859078', 'longitude': '10.598012' }, { 'name': 'Beni Khalled', 'latitude': '36.648240', 'longitude': '10.589766' }, { 'name': 'Montplaisir', 'latitude': '36.818307', 'longitude': '10.189319' }, { 'name': 'Mornag', 'latitude': '36.679725', 'longitude': '10.288208' }, { 'name': 'La Marsa Sidi Abdelaziz', 'latitude': '36.889594', 'longitude': '10.321619' }, { 'name': 'Korba', 'latitude': '36.570956', 'longitude': '10.857713' }, { 'name': 'Gafsa Hay Ennour', 'latitude': '34.428743', 'longitude': '8.782308' }, { 'name': 'Djerba-Midoun', 'latitude': '33.809291', 'longitude': '10.989723' }, { 'name': 'Mourouj 1', 'latitude': '36.738184', 'longitude': '10.207313' }, { 'name': 'Fahs', 'latitude': '36.376256', 'longitude': '9.902885' }, { 'name': 'Charguia I', 'latitude': '36.835657', 'longitude': '10.202212' }, { 'name': 'Sfax Poudriére', 'latitude': '34.753729', 'longitude': '10.776046' }, { 'name': 'Jemmal', 'latitude': '35.625117', 'longitude': '10.759733' }, { 'name': 'Manouba', 'latitude': '36.809075', 'longitude': '10.091897' }, { 'name': 'Rades', 'latitude': '36.773299', 'longitude': '10.273575' }, { 'name': 'Ennaser Crystal Palace ', 'latitude': '36.855904', 'longitude': '10.156069000000002' }, { 'name': 'Mjez El Beb ', 'latitude': '36.81185560459571', 'longitude': '9.609087999999929' }, { 'name': 'Teboulba', 'latitude': '35.638537', 'longitude': '10.969585999999936' }, { 'name': 'Cité El Ghazela ', 'latitude': '36.887770 ', 'longitude': '10.182530' }, { 'name': 'Jardin d\'El Menzah ', 'latitude': '36.857388 ', 'longitude': '10.127784' }, { 'name': 'Habib Bourguiba ', 'latitude': '36.799770 ', 'longitude': '10.178540' }, { 'name': 'Béja ', 'latitude': '36.81185560459571', 'longitude': '10.174985074218739' }, { 'name': 'Mahres ', 'latitude': '34.521747', 'longitude': '10.496653000000038' }, { 'name': 'Sfax Route Gabes ', 'latitude': '34.727694', 'longitude': '10.743404000000055' }, { 'name': 'Sahline ', 'latitude': '35.752313', 'longitude': '10.712859999999978' }, { 'name': 'Mateur ', 'latitude': '37.03649', 'longitude': '9.663589000000002' }, { 'name': 'Metlaoui ', 'latitude': '0', 'longitude': '0' }, { 'name': 'Metouia ', 'latitude': '33.958356 ', 'longitude': '10.004771' }, { 'name': 'Kalaa Kebira', 'latitude': '35.869148 ', 'longitude': '10.540885' }, { 'name': 'Bizerte 14 janvier ', 'latitude': '37.285407 ', 'longitude': '9.862762' }, { 'name': 'Borj Louzir ', 'latitude': '36.865069 ', 'longitude': '10.200291' }, { 'name': 'Djerba May', 'latitude': '33.803950 ', 'longitude': '10.882560' }, { 'name': 'Sfax route Ain ', 'latitude': '34.749026 ', 'longitude': '10.735471' }, { 'name': 'Mhammdia ', 'latitude': '36.676792 ', 'longitude': '10.156648' }, { 'name': 'Sfax Merkez Sahnoun ', 'latitude': '34.792483 ', 'longitude': '10.716193' }, { 'name': 'Mourouj 6 ', 'latitude': '36.710302 ', 'longitude': '10.213746' }, { 'name': 'Menzel jmil', 'latitude': '37.239371 ', 'longitude': '9.913220' }, { 'name': 'Grombalia ', 'latitude': '0', 'longitude': '0' }, { 'name': 'Sousse Jawhara ', 'latitude': '35.831194 ', 'longitude': '10.625460' }, { 'name': 'Menzel Bouzelfa ', 'latitude': '36.683985 ', 'longitude': '10.578109' }, { 'name': 'Jedaida', 'latitude': '36.836450 ', 'longitude': '9.941088' }, { 'name': 'Sousse Riad', 'latitude': '35.801313 ', 'longitude': '10.605767' }, { 'name': 'Tatouine Cité Ennozha', 'latitude': '32.936833 ', 'longitude': '10.451833' }, { 'name': 'Sbeitla ', 'latitude': '35.233083 ', 'longitude': '9.126278' }, { 'name': 'Barket Essahel', 'latitude': '36.405520 ', 'longitude': '10.566519' }, { 'name': 'Denden ', 'latitude': '36.802692 ', 'longitude': '10.113281' }, { 'name': 'Yasminet ', 'latitude': '36.738333 ', 'longitude': '10.241333' }, { 'name': 'Bardo Bouchoucha ', 'latitude': '36.802472 ', 'longitude': '10.148889' }, { 'name': 'Tabarka ', 'latitude': '36.955361 ', 'longitude': '8.758528' }, { 'name': 'Kairouan Echbilia ', 'latitude': '35.691641', 'longitude': '10.102339' }];

$(document).ready(function () {

    if (APP_ENV == 'production') {
        console.log = function () {};
    }

    $('.loader').addClass('hide');
    function showLoader() {
        var progress = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

        if (!progress) {
            $('.loader').removeClass('hide');
            $('body').css('overflow', 'hidden');
        } else {
            $('.progress-wrapper').removeClass('hide');
            $('body').css('overflow', 'hidden');
        }
    }
    function hideLoader() {
        var progress = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

        if (!progress) {
            $('.loader').addClass('hide');
            $('body').css('overflow', 'visible');
        } else {
            $('.progress-wrapper').addClass('hide');
            $('body').css('overflow', 'visible');
        }
    }

    $.ajaxSetup({
        error: function error(jqXHR) {
            hideLoader();
            hideLoader(true);

            if (/<html>/i.test(jqXHR.responseText)) sweetAlert('Oups!', 'Une erreur serveur s\'est produite, veuillez réessayer ultérieurement.', 'error');else $('#addExp').removeClass('disabled');
            sweetAlert({
                title: "Oups!",
                text: jqXHR.responseText,
                type: "error"
            });
        }
    });

    function closestLocation(targetLocation, locationData) {

        function vectorDistance(dx, dy) {
            return Math.sqrt(dx * dx + dy * dy);
        }

        function locationDistance(location1, location2) {
            var dx = location1.latitude - location2.latitude,
                dy = location1.longitude - location2.longitude;

            return vectorDistance(dx, dy);
        }

        return locationData.reduce(function (prev, curr, i, arr) {
            var prevDistance = locationDistance(targetLocation, prev),
                currDistance = locationDistance(targetLocation, curr);
            return prevDistance < currDistance ? prev : curr;
        });
    }

    function ipLookUp() {

        $.ajax('https://extreme-ip-lookup.com/json/', {
            error: function error(jqXHR) {
                hideLoader();
                hideLoader(true);
                sweetAlert({
                    title: "Oups!",
                    text: 'Veuillez activer la géolocalisation pour pouvoir jouer',
                    type: "error"
                });
            }
        }).then(function success(response) {
            console.log('User\'s Location Data is ', response);
            console.log('User\'s Country', response.country);
            initMap(response.lat, response.lon);
        }, function fail(data, status) {
            console.log('Request failed.  Returned status of', status);
        });
    }

    function onDragEnd() {
        var lngLat = marker.getLngLat();
        userPosition.longitude = lngLat.lng;
        userPosition.latitude = lngLat.lat;

        map.flyTo({
            center: [userPosition.longitude, userPosition.latitude],
            essential: false
        });
    }

    var mapboxToken = 'pk.eyJ1IjoiZGlnY3JvdyIsImEiOiJjazQ5OGNza2EwMm10M2dwYjM3OXE4bGh1In0.RL3oyZxe4Lz24X7qW_-q_g';
    mapboxgl.accessToken = mapboxToken;

    var map, marker;
    var userPosition = {};
    var nearestAgence = [];
    var shuffledLocations = [];

    if ($('body').hasClass('inscription')) {
        showLoader();
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function success(position) {
                console.log('latitude', position.coords.latitude, 'longitude', position.coords.longitude);
                initMap(position.coords.latitude, position.coords.longitude);
            }, function error(error_message) {
                // $('.form-wrapper').addClass('hide');
                hideLoader();
                sweetAlert({
                    title: "Veuillez activer la géolocalisation et rafraîchir pour pouvoir jouer",
                    // text: 'Veuillez activer la géolocalisation et rafraîchir pour pouvoir jouer',
                    type: "error",
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    allowEscapeKey: false
                });
            }, {
                enableHighAccuracy: true,
                maximumAge: 0
            });
        } else {
            console.log('geolocation is not enabled on this browser');
            // $('.form-wrapper').addClass('hide');
            sweetAlert({
                title: "Veuillez activer la géolocalisation et rafraîchir pour pouvoir jouer",
                // text: 'Veuillez activer la géolocalisation et rafraîchir pour pouvoir jouer',
                type: "error",
                showConfirmButton: false,
                allowOutsideClick: false,
                allowEscapeKey: false
            });
        }
    }

    function initMap(lat, lon) {

        hideLoader();
        $('#mapModal').modal('show');

        userPosition.longitude = lon;
        userPosition.latitude = lat;

        setTimeout(function () {
            map = new mapboxgl.Map({
                container: 'mapPosition', // container id
                style: 'mapbox://styles/digcrow/ck4de7lyx02by1clhmdbr4fe8',
                // style: 'mapbox://styles/mapbox/streets-v11',
                center: [lon, lat],
                minZoom: 8,
                zoom: 12
            });

            mapboxgl.setRTLTextPlugin('https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-rtl-text/v0.1.0/mapbox-gl-rtl-text.js');
            map.addControl(new MapboxLanguage({
                defaultLanguage: 'fr'
            }));

            marker = new mapboxgl.Marker({
                draggable: true
            }).setLngLat([lon, lat]).addTo(map);

            marker.on('dragend', onDragEnd);
        }, 300);
    }

    var activeUser = localStorage.getItem('userTel');

    $('#confirmPosition').click(function () {

        $('#mapModal').modal('hide');
        var map2 = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/digcrow/ck4de7lyx02by1clhmdbr4fe8',
            center: [userPosition.longitude, userPosition.latitude],
            minZoom: 4,
            zoom: 13
        });

        map2.addControl(new MapboxLanguage({
            defaultLanguage: 'fr'
        }));

        new mapboxgl.Marker({
            draggable: false
        }).setLngLat([userPosition.longitude, userPosition.latitude]).addTo(map2);

        for (var i = 0; i <= 2; i++) {
            nearestAgence[i] = closestLocation(userPosition, agencesLocation);
            shuffledLocations[i] = closestLocation(userPosition, agencesLocation);
            agencesLocation.splice(agencesLocation.indexOf(nearestAgence[i]), 1);
        }

        if (activeUser !== null && activeUser !== 'undefined') {
            $.ajax({
                url: '/canPlay',
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: { tel: activeUser }
            }).done(function (response) {

                if (response.success) {

                    $('.gameCanvas').removeClass('hide');
                    $('.form-wrapper').addClass('hide');
                    $('.map-container').addClass('hide');
                    game();
                    return true;
                } else {
                    if (response.code === 4) {
                        $('.form-wrapper').addClass('hide');

                        if (response.won === 1) $('.msg-wrapper').removeClass('hide');else $('.msg-wrapper-error').removeClass('hide');

                        $('.map-container').addClass('hide');
                        return sweetAlert({ title: 'Merci!', text: response.message, type: 'info' });
                    }
                }
            });
        }
    });

    $('#inscription').submit(function (e) {

        var $this = $(this);

        if ($this.hasClass('disabled')) return false;

        showLoader(true);
        $this.addClass('disabled');

        $.ajax({
            url: $this.attr('action'),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function (response) {

            $this.removeClass('disabled');
            hideLoader(true);
            if (response.success) {

                $('.gameCanvas').removeClass('hide');
                $('.form-wrapper').addClass('hide');
                $('.map-container').addClass('hide');

                localStorage.setItem('userTel', response.userTel);
                activeUser = response.userTel;

                game();
                return true;
            }
            if (response.code === 4) {
                $('.form-wrapper').addClass('hide');

                if (response.won === 1) $('.msg-wrapper').removeClass('hide');else $('.msg-wrapper-error').removeClass('hide');

                $('.map-container').addClass('hide');
                return sweetAlert({ title: 'Merci!', text: response.message, type: 'info' });
            }

            return sweetAlert({ title: 'Oups!', text: response.message, type: 'error' });
        });

        e.preventDefault();
    });

    function shuffle(array) {
        var currentIndex = array.length,
            temporaryValue,
            randomIndex;
        var localArray = array;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = localArray[currentIndex];
            localArray[currentIndex] = localArray[randomIndex];
            localArray[randomIndex] = temporaryValue;
        }

        return localArray;
    }

    function game() {

        shuffledLocations = shuffle(shuffledLocations);

        var targets = $(".target");
        var overlapThreshold = "10%";

        var boxes = $(".bankItem");
        $.each(boxes, function (i, e) {
            var position = $(e).offset();
            e.originalLeft = position.left - 8;
            e.originalTop = position.top - 8;
            e.index = i;
            $('.bank__name', e).html(shuffledLocations[i].name);
        });

        Draggable.create(".bankItem", {
            bounds: ".gameArea",
            edgeResistance: 0.65,
            allowNativeTouchScrolling: false,
            type: "x,y",
            throwProps: true,
            onDragStart: function onDragStart(e) {
                if (e.target.targetAttachedTo) e.target.targetAttachedTo.removeClass("occupied");
                $('.target').removeClass('a-right').removeClass('a-wrong');
            },

            // changes the colour of the targets whilst dragging to give feedback that a
            // dragged object is going to snap to it
            onDrag: function onDrag(e) {
                for (var i = 0; i < targets.length; i++) {
                    if (!$(targets[i]).hasClass('occupied')) if (this.hitTest(targets[i], overlapThreshold)) {
                        $(targets[i]).addClass("showOver");
                    } else {
                        $(targets[i]).removeClass("showOver");
                    }
                }
            },

            onDragEnd: function onDragEnd(e) {
                var snapMade = false;
                for (var i = 0; i < targets.length; i++) {
                    if (this.hitTest(targets[i], overlapThreshold)) {

                        if (!$(targets[i]).hasClass("occupied")) {

                            var p = $(targets[i]).position();

                            $(targets[i]).addClass("occupied");
                            placed++;

                            // tween onto target
                            TweenLite.to(e.target, 0.2, { left: p.left + parseInt($(targets[i]).css('margin-left')) + 10, top: p.top + 10, x: 0, y: 0 });
                            $(e.target).attr('data-order', $(targets[i]).data('index')).attr('data-placed', 'true');

                            if (e.target.targetAttachedTo != $(targets[i]) && e.target.targetAttachedTo != undefined) {
                                e.target.targetAttachedTo.removeClass("occupied");
                                e.target.targetAttachedTo = undefined;
                            }

                            e.target.targetAttachedTo = $(targets[i]);
                            snapMade = true;

                            var placed = 0;
                            for (var i = 0; i < boxes.length; i++) {
                                if ($(boxes[i]).attr('data-placed') === 'true') placed++;
                            }
                            if (placed === 3) {
                                $('#checkResut').removeClass('disabled').removeAttr('disabled');
                            } else {
                                $('#checkResut').addClass('disabled').attr('disabled');
                            }
                        }
                    }
                }

                if (!snapMade) {
                    if (e.target.targetAttachedTo != undefined) {
                        e.target.targetAttachedTo.removeClass("occupied");
                    }
                    TweenLite.to(e.target, 0.1, { x: 0, y: 0, top: e.target.originalTop - parseInt($('.gameArea').css('padding-top')) + 30, left: e.target.originalLeft });
                    $(e.target).attr('data-order', '').attr('data-placed', 'false');
                    var placed = 0;
                    for (var i = 0; i < boxes.length; i++) {
                        if ($(boxes[i]).attr('data-placed') === 'true') placed++;
                    }
                    if (placed === 3) {
                        $('#checkResut').removeClass('disabled').removeAttr('disabled');
                    } else {
                        $('#checkResut').addClass('disabled').attr('disabled');
                    }
                }
            }
        });

        var attempts = 2;

        $('#gameForm').submit(function (e) {
            showLoader(true);
            e.preventDefault();
            var $this = $(this);

            if ($this.hasClass('disabled')) return false;
            $this.addClass('disabled');

            attempts--;
            var rep = '';
            var current = -1;

            for (var i = 0; i < boxes.length; i++) {
                if ($('.bankItem[data-order="' + (i + 1) + '"] div').html() !== nearestAgence[i].name) {
                    $('.target[data-index="' + (i + 1) + '"').addClass('a-wrong');
                } else {
                    $('.target[data-index="' + (i + 1) + '"').addClass('a-right');
                }
            }

            for (var i = 0; i < boxes.length; i++) {

                rep += ' ____ ' + nearestAgence[i].name;

                if ($('.bankItem[data-order="' + (i + 1) + '"] div').html() !== nearestAgence[i].name) {
                    $this.removeClass('disabled');
                    hideLoader(true);
                    $this[0].reset();
                    return sweetAlert({
                        title: 'Oups !',
                        text: 'Votre réponse est incorrecte',
                        type: 'warning'
                    });
                }
                current = i;
            }

            if (activeUser !== null) {
                $('#inpuPhone').val(activeUser);
            } else {
                $('#inpuPhone').val($('#inputTel').val());
            }
            $('#inpuGame').val(rep);
            $('#inpuCurrent').val(current);

            if (attempts < 1) {
                $.ajax({
                    url: $this.attr('action'),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false
                }).done(function (response) {

                    $this.removeClass('disabled');
                    hideLoader(true);
                    if (response.success) {
                        $this[0].reset();
                        $('.gameCanvas').addClass('hide');
                        $('.msg-wrapper').removeClass('hide');
                        return true;
                    }
                    $('.gameCanvas').addClass('hide');
                    $('.msg-wrapper-error').removeClass('hide');
                    return sweetAlert({ title: 'Oups!', text: response.message, type: 'error' });
                });
                return false;
            }

            $.ajax({
                url: $this.attr('action'),
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: new FormData(this),
                processData: false,
                contentType: false
            }).done(function (response) {

                $this.removeClass('disabled');
                hideLoader(true);
                if (response.success) {
                    $this[0].reset();
                    $('.gameCanvas').addClass('hide');
                    $('.msg-wrapper').removeClass('hide');
                    return true;
                }
                return sweetAlert({ title: 'Oups!', text: response.message, type: 'error' });
            });
        });
    }
});
//# sourceMappingURL=main.js.map
