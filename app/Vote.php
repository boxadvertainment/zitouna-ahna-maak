<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    //
    public function participant (){
        return $this->belongsTo('App\Participant');
    }

    public function voter (){
        return $this->belongsTo('App\Voter');
    }
    public function getVoteCount($participant_id){
        return Vote::where('participant_id', $participant_id)->count();
    }
}
