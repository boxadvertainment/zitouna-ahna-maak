<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Vote;

class Post extends Model
{
    protected $table = 'posts';

    public function participant()
    {
        return $this->belongsTo('App\Participant');
    }

    public function getVoteCount(){
        return Vote::where('participant_id', $this->participant_id )->count();
    }

    public function getNumberByStatus($status)
    {
        return Post::where('status', $status)->count();
    }

    public function getListByStatus($status)
    {
        return Post::where('status', $status)->get();
    }


    public function getPostImage(){
        return sprintf(
            'https://img.youtube.com/vi/%s/0.jpg',
            $this->video_link
        );
    }
}
