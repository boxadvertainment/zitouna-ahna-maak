<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::get('/', 'AppController@index');
//Route::get('/inscription', 'AppController@inscription');
//Route::post('/saveForm' , 'AppController@saveInscription');
//Route::post('/saveGame' , 'AppController@saveGame');
//Route::get('/comment-participer' , 'AppController@howTo');
//Route::get('/gain' , 'AppController@gain');

Route::post('/checkUser' , 'AppController@checkUser');
Route::post('/canPlay' , 'AppController@canPlay');


Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::auth();
    Route::get('/', 'AdminController@index');
    Route::get('/winners', 'AdminController@getWinners');
});

//Route::post('auth/facebookLogin/{offline?}', 'Auth\AuthController@facebookLogin');
//Route::post('signup', 'AppController@signup');
