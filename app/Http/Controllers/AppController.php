<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Participant;
use App\Games;

class AppController extends Controller{


    public function index(){
        return view('index');
    }
    public function inscription(){
        return view('inscription');
    }
    public function howTo(){
        return view('howto');
    }
    public function gain(){
        return view('gain');
    }


    function saveGame(Request $request){

        $participant = Participant::where('tel', $request->input('tel') )->first();

        $g = Games::where('participant_id', $participant->id )
            ->whereDate('created_at', '=', Carbon::today())
            ->count();

        if($g === 0 ){

            $game = new Games;
            $game->participant_id = $participant->id;

            if($request->input('current') == '2'){
                $game->won = true;
            }else{
                $game->won = false;
            }

            if ( !$game->save() ) {
                return response()->json([
                    'success'   => false,
                    'code'      => '3',
                    'message'   => "Un problème est survenu lors de l'enregistrement de votre participation. Veuillez réessayer. Merci"
                ]);
            }

            if($request->input('current') == '2'){
                return response()->json([
                    'success'   => true,
                    'message'   =>  "Vous ferez parti du tirage au sort!"
                ]);
            }

            return response()->json([
                'success'   => false,
                'message'   =>  "Retentez votre chance demain."
            ]);

        }

        return response()->json([
            'success'   => false,
            'code'      => 4,
            'message'   => "Vous avez déjà participé. Retentez votre chance demain."
        ]);

    }


    function canPlay(Request $request){
        $participant = Participant::where('tel', $request->input('tel') )->first();
        if($participant <> null ){
            $game = Games::where('participant_id', $participant->id )
                ->whereDate('created_at', '=', Carbon::today())->first();

            if ($game <> null ) {
                return response()->json([
                    'success'   => false,
                    'won'      => $game->won,
                    'code'      => 4,
                    'message'   => "Vous avez déjà participé. Retentez votre chance demain."
                ]);
            }else{
                return response()->json([ 'success'   => true ]);
            }
        }
        return response()->json([ 'success'   => false ]);
    }

    public function checkUser(Request $request){
        $participant = Participant::where('tel', $request->input('tel') )->first();

        if($participant <> null){
            return response()->json([ 'success'   => true ]);
        }

        return response()->json([ 'success'   => false ]);
    }


    public function saveInscription(Request $request){

        $participant = Participant::where('tel', $request->input('tel') )->first();
        if($participant <> null ){
            $game = Games::where('participant_id', $participant->id )
                ->whereDate('created_at', '=', Carbon::today())->first();

            if ($game <> null ) {
                return response()->json([
                    'success'   => false,
                    'won'      => $game->won,
                    'code'      => 4,
                    'message'   => "Vous avez déjà participé. Retentez votre chance demain."
                ]);
            }
        }


        if ($participant === null) {

            $validator = \Validator::make($request->all(), [
                'name'              => 'required',
                'city'              => 'required',
                'tel'               => 'required|regex:/^[0-9]{8}$/|unique:participants'
            ]);

            $validator->setAttributeNames([
                'name'               => 'Nom et Prénom',
                'city'               => 'Ville',
                'tel'                => 'Téléphone',
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'code' => 102, 'message' => implode('<br>', $validator->errors()->all())]);
            }

            $participant = new Participant;

            $participant->tel = $request->input('tel');
            $participant->name = $request->input('name');
            $participant->city = $request->input('city');
            $participant->ip = $request->ip();


            if ( !$participant->save() ) {
                return response()->json([
                    'success'   => false,
                    'code'      => '3',
                    'message'   => "Un problème est survenu lors de l'enregistrement de votre participation. Veuillez réessayer. Merci"
                ]);
            }

            return response()->json([
                'success'   => true,
                'userTel'   => $request->input('tel'),
                'message'   => "Vous ferez parti du tirage au sort !"
            ]);
        }



        return response()->json([
            'success'   => true,
            'userTel'   => $request->input('tel'),
            'message'   => "Vous ferez parti du tirage au sort !"
        ]);
    }

}
