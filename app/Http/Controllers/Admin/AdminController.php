<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Participant;
use App\Games;

class AdminController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

    public function getWinners(){
        $nbParticipant = Participant::all()->count();
        $nbGames = Games::where('won','=', '1')->count();
        $games = Participant::leftJoin('games', 'participants.id', '=', 'games.participant_id')->where('won','=', '1')->get();

        return view('admin.winners')->with( [
            'nbParticipant' => $nbParticipant,
            'games' => $games,
            'nbGames' => $nbGames
        ]);
    }

    public function index(){
        $nbParticipant = Participant::all()->count();
        $nbGames = Games::all()->count();
        $games = Participant::leftJoin('games', 'participants.id', '=', 'games.participant_id')->get();

        return view('admin.index')->with( [
            'nbParticipant' => $nbParticipant,
            'games' => $games,
            'nbGames' => $nbGames
        ]);
    }

}
